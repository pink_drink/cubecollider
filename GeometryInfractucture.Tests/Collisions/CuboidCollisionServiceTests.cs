﻿using GeometryInfractucture.Collisions;
using GeometryInfractucture.Primitives;
using GeometryInfractucture.Shapes.Cubes;
using Moq;
using NUnit.Framework;

namespace GeometryInfractucture.Tests.Collisions
{
    [TestFixture]
    public class CuboidCollisionServiceTests
    {
        private Mock<ICuboidFactory> _cuboidFactoryMock;
        private CuboidCollisionService _cuboidCollisionService;

        [SetUp]
        public void SetUp()
        {
            _cuboidFactoryMock = new Mock<ICuboidFactory>();
            _cuboidCollisionService = new CuboidCollisionService(_cuboidFactoryMock.Object);
        }
    
        [TestCase(0,0,0,10,5)]
        public void CheckCreateCuboidWasColledForCollidedCubes(decimal x, decimal y, decimal z, 
            decimal sideFirstCube, decimal sideSecondCube)
        {
            var vertex = CreateVertex(x, y, z);
            var cubeFirst = CreateCuboid(vertex , sideFirstCube);
            var cubeSecond = CreateCuboid(vertex , sideSecondCube);
            
            _cuboidCollisionService.GetCollisionOrNull(cubeFirst, cubeSecond);
            
            _cuboidFactoryMock.Verify(fact=>fact.Create(null, 5, 5, 5), Times.Once);
        }

        [TestCase(-2, -4, -1, 8, -6, -8, -1, 2)]
        public void CheckCuboidWasCalculadedCorrectlyForDifferentCenters(decimal xFirst, decimal yFirst, decimal zFirst, 
            decimal sideFirst, decimal xSecond, decimal ySecond, decimal zSecond, 
            decimal sideSecond)
        {
            var vertexFirst = CreateVertex(xFirst, yFirst, zFirst);
            var vertexSecond = CreateVertex(xSecond, ySecond, zSecond);

            var cubeFirst = CreateCuboid(vertexFirst, sideFirst);
            var cubeSecond = CreateCuboid(vertexSecond, sideSecond);

            var cuboid = _cuboidCollisionService.GetCollisionOrNull(cubeFirst, cubeSecond);

            _cuboidFactoryMock.Verify(fact=>fact.Create(null, 1, 1, 2), Times.Once);
        }

        [Test]
        public void CheckVolumeCalculatedCorretly()
        {
            var vertex = CreateVertex(0, 0, 0);
            var cubeFirst = CreateCuboid(vertex , 1);
            var cubeSecond = CreateCuboid(vertex , 1);
            _cuboidFactoryMock.Setup(fact => fact.Create(It.IsAny<Vertex>(), It.IsAny<decimal>(),
                It.IsAny<decimal>(), It.IsAny<decimal>())).Returns(new Cuboid(vertex, 5, 5, 5));

            var cuboid = _cuboidCollisionService.GetCollisionOrNull(cubeFirst, cubeSecond);

            Assert.AreEqual(125, cuboid.GetVolume());
        }


        private Cuboid CreateCuboid(Vertex center, decimal side)
        {
            return new Cube(center, side);
        }

        private  Vertex CreateVertex(decimal x, decimal y, decimal z)
        {
            return new Vertex(x,y,z);
        }
    }
}