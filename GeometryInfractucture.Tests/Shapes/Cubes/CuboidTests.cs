﻿using GeometryInfractucture.Primitives;
using GeometryInfractucture.Shapes.Cubes;
using NUnit.Framework;

namespace GeometryInfractucture.Tests.Shapes.Cubes
{
    [TestFixture]
    public class CuboidTests
    {
        [SetUp]
        public void Setup()
        {
            _center = new Vertex(0, 0, 0);
            _edgeA = 0;
            _edgeB = 0;
            _edgeC = 0;
        }

        private static Vertex _center;
        private static int _edgeA;
        private static int _edgeB;
        private static int _edgeC;

        [TestCase(50, 30, 10)]
        [TestCase(10, 20, 30)]
        public void ReturnsVertexAFromEdges(int x, int y, int z)
        {
            _edgeA = x;
            _edgeB = y;
            _edgeC = z;

            var cuboid = CreateCuboid();

            Assert.AreEqual(-x / 2, cuboid.A.X);
            Assert.AreEqual(-y / 2, cuboid.A.Y);
            Assert.AreEqual(-z / 2, cuboid.A.Z);
        }

        [TestCase(10, 20, 30)]
        [TestCase(-10, 0, 10)]
        public void ReturnsVertexAWithCenter(int x, int y, int z)
        {
            _center = new Vertex(x, y, z);
            _edgeA = 50;
            _edgeB = 50;
            _edgeC = 50;

            var cuboid = CreateCuboid();

            Assert.AreEqual(x - _edgeA / 2, cuboid.A.X);
            Assert.AreEqual(y - _edgeB / 2, cuboid.A.Y);
            Assert.AreEqual(z - _edgeC / 2, cuboid.A.Z);
        }

        [TestCase(20, 10, 10)]
        public void ReturnsVertexBxCyEzWithCenter(int x, int y, int z)
        {
            _center = new Vertex(x, y, z);
            _edgeA = 40;
            _edgeB = 50;
            _edgeC = 10;

            var cuboid = CreateCuboid();

            Assert.AreEqual(x + _edgeA / 2, cuboid.Bx);
            Assert.AreEqual(y + _edgeB / 2, cuboid.Cy);
            Assert.AreEqual(z + _edgeC / 2, cuboid.Ez);
        }

        private static Cuboid CreateCuboid()
        {
            return new Cuboid(_center, _edgeA, _edgeB, _edgeC);
        }
    }
}