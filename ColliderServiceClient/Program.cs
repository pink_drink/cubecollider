﻿using System;


namespace ColliderServiceClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var firstCubeCenter = GetUserInput(false, "Enter 3 numbers (x, y, z) as center of first cube", 3);
            var firstCubeSide = GetUserInput(false, "Enter side length for first cube", 1);


            var secondCubeCenter = GetUserInput(false, "Enter 3 numbers (x, y, z) as center of second cube", 3);
            var secondCubeSide = GetUserInput(false, "Enter side length for second cube", 1);

            var client = new ServiceClient();

            var cubeFirst = client.CreateCube(firstCubeCenter, firstCubeSide);
            var cubeSecond = client.CreateCube(secondCubeCenter, secondCubeSide);

            var volume = client.GetCollidedVolume(cubeFirst, cubeSecond);
            Console.WriteLine(" Collided volume : " + volume);
            Console.ReadKey();
        }

        private static decimal[] GetUserInput(bool correctInput, string message, int expectedNumber)
        {
            Console.WriteLine(message);
            string input = null;

            while (!correctInput)
            {
                input = Console.ReadLine();
                var ifNumbersCountValid = IfNumbersCountValid(input, expectedNumber);
                var ifNumbersFormatValid = IfNumbersFormatValid(input);
                correctInput = ifNumbersCountValid && ifNumbersFormatValid;
            }

            return ParseInputToDecimal(input);
        }

        private static bool IfNumbersFormatValid(string input)
        {
            var inputs = input.Split(" ");

            foreach (var t in inputs)
            {
                if (!decimal.TryParse(t, out _))
                {
                    Console.WriteLine("invalid format of input");
                    return false;
                }
            }

            return true;
        }

        private static decimal[] ParseInputToDecimal(string input)
        {
            var inputs = input.Split(" ");
            decimal[] points = new decimal[inputs.Length];

            for  (int i = 0; i< inputs.Length; i++)
            {
                points[i] = decimal.Parse(inputs[i]);
                
            }
            return points;
        }

        private static bool IfNumbersCountValid(string input, int expectedNumber)
        {
            var inputs = input.Split();

            if (inputs.Length != expectedNumber)
            {
                Console.WriteLine("invalid numbers of input. Expected {0} but was less or more then {0}", expectedNumber);
                return false;
            }

            return true;
        }
    }
}
