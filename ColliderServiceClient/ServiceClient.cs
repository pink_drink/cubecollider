﻿using Autofac;
using GeometryInfractucture;
using GeometryInfractucture.Collisions;
using GeometryInfractucture.Primitives;
using GeometryInfractucture.Shapes.Cubes;

namespace ColliderServiceClient
{
    public class ServiceClient
    {
        private readonly ICuboidCollisionService _cuboidCollisionService;
        private readonly ICuboidFactory _cuboidFactory;


        public ServiceClient()
        {
            var appBootstrap = new AutofaqBootstrapp();
            var container = appBootstrap.Build();
            _cuboidCollisionService = container.Resolve<ICuboidCollisionService>();
            _cuboidFactory = container.Resolve<ICuboidFactory>();
        }

        public decimal GetCollidedVolume(ICuboid cube1, ICuboid cube2)
        {

            var cuboid = _cuboidCollisionService.GetCollisionOrNull(cube1, cube2);

            return cuboid.GetVolume();
        }

        public  ICuboid CreateCube(decimal[] xyz, decimal[] side)
        {
            var vertex = CreateVertex(xyz);

            var cuboid = _cuboidFactory.Create(vertex, side[0]);

            return cuboid;
        }

        private  Vertex CreateVertex(decimal[] xyz)
        {
            return new Vertex(xyz[0], xyz[1], xyz[2]);
        }
    }
}
