﻿using GeometryInfractucture.Primitives;

namespace GeometryInfractucture.Shapes.Cubes
{
    internal class Cuboid : IShape, ICuboid
    {
        private readonly IVertex _center;
        private readonly decimal _edgeX;
        private readonly decimal _edgeY;
        private readonly decimal _edgeZ;

        public IVertex A { get; private set; }
        public decimal Bx { get; private set; }
        public decimal Cy { get; private set; }
        public decimal Ez { get; private set; }

        public Cuboid(
            IVertex center,
            decimal edgeX,
            decimal edgeY,
            decimal edgeZ)
        {
            _center = center;
            _edgeX = edgeX;
            _edgeY = edgeY;
            _edgeZ = edgeZ;

            SetVertexes();
        }


        private void SetVertexes()
        {
            A = new Vertex(_center.X - _edgeX / 2, _center.Y - _edgeY / 2, _center.Z - _edgeZ / 2);
            Bx = _center.X + _edgeX / 2;
            Cy = _center.Y + _edgeY / 2;
            Ez = _center.Z + _edgeZ / 2;
        }

        public decimal GetVolume()
        {
            return _edgeX * _edgeY * _edgeZ;
        }
    }
}