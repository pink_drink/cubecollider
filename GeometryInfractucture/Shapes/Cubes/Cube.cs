﻿using GeometryInfractucture.Primitives;

namespace GeometryInfractucture.Shapes.Cubes
{
    internal class Cube : Cuboid
    {
        public Cube(IVertex center, decimal edge)
            : base(center, edge, edge, edge)
        {
        }
    }
}