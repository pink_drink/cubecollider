﻿using GeometryInfractucture.Primitives;

namespace GeometryInfractucture.Shapes.Cubes
{
    public interface ICuboidFactory
    {
        ICuboid Create(
            IVertex center,
            decimal edgeA,
            decimal edgeB,
            decimal edgeC);

        ICuboid Create(
            IVertex center,
            decimal edge);
    }
}