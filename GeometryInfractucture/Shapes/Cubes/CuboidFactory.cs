﻿using GeometryInfractucture.Primitives;

namespace GeometryInfractucture.Shapes.Cubes
{
    internal class CuboidFactory : ICuboidFactory
    {
        public ICuboid Create(IVertex center, decimal edgeA, decimal edgeB, decimal edgeC)
        {
            return new Cuboid(center, edgeA, edgeB, edgeC);
        }

        public ICuboid Create(IVertex center, decimal edge)
        {
            return new Cube(center, edge);
        }
    }
}