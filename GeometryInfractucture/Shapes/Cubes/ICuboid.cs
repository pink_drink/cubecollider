﻿using GeometryInfractucture.Primitives;

namespace GeometryInfractucture.Shapes.Cubes
{
    public interface ICuboid : IShape
    {
        IVertex A { get; }
        decimal Bx { get; }
        decimal Cy { get; }
        decimal Ez { get; }
    }
}