﻿namespace GeometryInfractucture.Shapes
{
    public interface IShape
    {
        decimal GetVolume();
    }
}
