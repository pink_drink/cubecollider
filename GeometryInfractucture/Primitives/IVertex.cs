﻿namespace GeometryInfractucture.Primitives
{
    public interface IVertex
    {
        decimal X { get; }
        decimal Y { get; }
        decimal Z { get; }
    }
}
