﻿namespace GeometryInfractucture.Primitives
{
    public class Vertex : IVertex
    {
        public Vertex(
            decimal x,
            decimal y,
            decimal z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public decimal X { get; }
        public decimal Y { get; }
        public decimal Z { get; }
    }
}