﻿using System.Runtime.CompilerServices;
using Autofac;
using GeometryInfractucture.Collisions;
using GeometryInfractucture.Shapes.Cubes;

[assembly: InternalsVisibleTo("GeometryInfractucture.Tests")]

namespace GeometryInfractucture
{
    public class AutofaqBootstrapp
    {
        public IContainer Build()
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterType<CuboidFactory>().As<ICuboidFactory>().SingleInstance();

            builder.RegisterType<CuboidCollisionService>().As<ICuboidCollisionService>().SingleInstance();
            return builder.Build();
        }
    }
}
