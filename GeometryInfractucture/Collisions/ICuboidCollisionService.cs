﻿using GeometryInfractucture.Shapes.Cubes;

namespace GeometryInfractucture.Collisions
{
    public interface ICuboidCollisionService
    {
        ICuboid GetCollisionOrNull(ICuboid firstCuboid, ICuboid secondCuboid);
    }
}