﻿using System;
using GeometryInfractucture.Shapes.Cubes;

namespace GeometryInfractucture.Collisions
{
    public class CuboidCollisionService : ICuboidCollisionService
    {
        private readonly ICuboidFactory _cuboidFactory;

        public CuboidCollisionService(ICuboidFactory cuboidFactory)
        {
            _cuboidFactory = cuboidFactory;
        }

        public ICuboid GetCollisionOrNull(ICuboid firstCuboid, ICuboid secondCuboid)
        {
            if (!IsCollided(firstCuboid,secondCuboid))
            {
                return null;
            }

            decimal edgeAx = Math.Max(secondCuboid.A.X, firstCuboid.A.X);
            decimal edgeBx = Math.Min(secondCuboid.Bx, firstCuboid.Bx);

            decimal edgeAy = Math.Max(secondCuboid.A.Y, firstCuboid.A.Y);
            decimal edgeCy = Math.Min(secondCuboid.Cy, firstCuboid.Cy);

            decimal edgeAz = Math.Max(secondCuboid.A.Z, firstCuboid.A.Z);
            decimal edgeEz = Math.Min(secondCuboid.Ez, firstCuboid.Ez);

            var edgeX = GetEdge(edgeAx, edgeBx);
            var edgeY = GetEdge(edgeAy, edgeCy);
            var edgeZ = GetEdge(edgeAz, edgeEz);

            var cuboid = _cuboidFactory.Create(null, edgeX, edgeY, edgeZ);
            return cuboid;
        }

        private decimal GetEdge(decimal edgeStart, decimal edgeEnd)
        {
            decimal edge = Math.Abs(edgeStart - edgeEnd);
            return edge;
        }

        private bool IsCollided(ICuboid firstCuboid, ICuboid secondCuboid)
        {
            if (secondCuboid.A.X >= firstCuboid.Bx || secondCuboid.Bx <=firstCuboid.A.X) 
            {
                return false;
            }

            if (secondCuboid.A.Z >= firstCuboid.Ez || secondCuboid.Ez <= firstCuboid.A.Z)
            {
                return false;
            }

            if (secondCuboid.A.Y >= firstCuboid.Cy || secondCuboid.Cy <= firstCuboid.A.Y)
            {
                return false;
            }

            return true;
        }
    }
}
